# challenge-deobfuscator (cf-debug)
CLI tool that helps debug Cloudflare's Anti-Scrape Shield

### Install
```
npm i -g cf-debug
```

### Usage
```sh
# Debug local html
cf-debug page.html
# Debug local js
cf-debug script.js
# Fetch and debug
cf-debug http://example-site.dev
# Save to file
cf-debug https://example-site.dev > challenge.html
# Don't remove elements
cf-debug http://example-site.dev --raw > out.html
# Log recent changes to challenges and exit
cf-debug --changes
# Show replacements, etc.
cf-debug https://example-site.dev --verbose
```

### Frequent updates
It's best to just clone this repo and link with npm
```sh
git clone https://github.com/pro-src/cf-debug
cd cf-debug
npm link
```

Update to the latest revision
```sh
git pull && npm link
```

### Sample
***Before***: https://gist.github.com/pro-src/fbf03cba4a8f6517395f7bc47dd494a7#gistcomment-2875805

***After***: https://gist.github.com/pro-src/fbf03cba4a8f6517395f7bc47dd494a7#gistcomment-2875806
