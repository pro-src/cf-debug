#!/usr/bin/env node

var fs = require('fs');
var path = require('path');
var request = require('request');
var URL = require('url').URL;
var cloudscraper = require('cloudscraper');
var chalk = require('chalk');

// npm i --save js-beautify cli-highlight
var beautify = require('js-beautify');
var highlight = require('cli-highlight').highlight

var target = process.argv[2];
var raw = process.argv.indexOf('--raw') !== -1;
var changes = process.argv.indexOf('--changes') !== -1;
var verbose = process.argv.indexOf('--verbose') !== -1;

var notices = [
  { date: '2019-04-03T12:04:21.591Z', message: 'g = String.fromCharCode' },
  { date: '2019-04-10T18:10:31.026Z', message: 'e = atob' }
];

if (changes) {
  news();
}

if (/^https?:\/\//.test(target)) {
  var options = Object.assign({}, cloudscraper.defaultParams);
  delete options.headers['Host'];
  options.uri = target;

  request(options, function (error, res, body) {
    if (error) {
      throw error;
    }

    main(body);
  });
} else {
  main(fs.readFileSync(path.resolve(target), 'utf8'));
}

function main(html) {
  html = html.toString('utf8');
  var scripts = [];

  var lang = /<(?:html|style|script)/i.test(html) !== -1 ? 'html' : 'js';

  if (!raw && lang === 'html') {
    // Remove <style>
    html = html.replace(/<style(?:>|\s+[^<>]*>)[\S\s]+?<\/style>/i, '');
    // Remove attribution
    html = html.replace(/<div\s+[^<>]*?class=["']?attribution[\S\s]+?<\/div>/gi, '')
    // Remove bubbles
    html = html.replace(/<div class="bubbles"><\/div>/gi, '');
    // Remove data-translate
    html = html.replace(/<(h1|span|p)\s+data-translate=[^<]+?<\/\1>/gi, '');
    // Remove H1-H6
    html = html.replace(/<(h[1-6])>[^<]*?<\/\1>/gi, '');
    // Remove meta
    html = html.replace(/<meta [^<]+?\/>/gi, '');
    // Remove title
    html = html.replace(/<title>Just a moment\.\.\.<\/title>/gi, '');
    // Remove noscript
    html = html.replace(/<noscript>[^<]*?<\/noscript>/gi, '');
    // Remove http://macinline.com/answeradenoidal.php
    html = html.replace(/<a href=["']?http:\/\/macinline\.com\/answeradenoidal\.php[^<]+?<\/a>/gi, '');
    // Cleanup
    var orig;
    while (orig !== html) {
      orig = html;
      // Empty div's
      html = html.replace(/<div(?:\s+id=["']cf-content["'])?(?:\s+style=["']display:\s*none;?["'])?\s*>\s*<\/div>/gi, '');
      // Replace two or more newlines with a single newline
      html = html.replace(/(?:\s*\r?\n){2,}/g, '\n');
    }
  }

  html = simplify(html);
  html = beautify[lang](html, { parser: 'babel' });

  console.log(highlight(html));
}

function atob(str) {
  return Buffer.from(str, 'base64').toString('binary');
}

function replacer(expr) {
  try {
   var result = eval(expr);
   return /[A-Za-z]/.test('' + result) || result === '.'
     ? '"' + result + '"' : String(result);
  }
  catch(e) {
    return expr;
  }
}

function getRegex(js) {
  var letter;
  var match;

  // Shorthand for new RegExp
  var re = function (pattern, flags = 'g') { return new RegExp(pattern, flags); };
  var parens = function (pattern = '') { return '\\(' + pattern + '\\)'; };
  // Shorthand for brackets
  var brkts = function (pattern = '') { return '\\[' + pattern + '\\]'; };
  // Surround with single or double quotes
  var quote = function (pattern, $ = 1) { return '(["\'])' + pattern + '\\' + $; };

  var operators = '\\+\\-\\/!=';
  var op = '[' + operators + ']';
  var math = '(?:\\s*' + op + '*' + '\\s*\\d)+';
  var num = '\\d+';
  var base64 = '[A-Za-z0-9\\/+=]+?';
  var types = [ num, 'true','false', 'undefined', '""', '\'\'', 'NaN', brkts() ];

  types = '(?:' + types.join('|') + ')';

  // Any of these can be disabled by commenting them out
  // It should still produce functionally equivalent code
  var regex = [
    // Opening paren followed by operator(s) and empty brackets
    re('(?<=[([:=]\\s*' + op + '?)(?:\\s*' + op + '*(?:' + brkts() + '(?=[^[]))+)+'),
    // Opening paren followed by operator(s) and e.g. (1234)
    re('(?<=[(' + op + '\\s?)(?:\\s*' + op + '*' + parens(num) + ')+'),
    // Operator followed by math
    re('(?<=[(>:=]\\s?)' + math),
    // Opening paren followed by operator(s) and e.g. (0 + Array)[10]
    re('(?<=[(' + op + '\\s?)\\+*(?:\\s*' + parens(types + op + types) + '(?:' + brkts(types) + ')?)+'),
    // Operator followed by non-empty parens(no parens) and maybe [1234]
    re('(?<=' + op + '\\s*)' + parens('[^()]*?') + '(?:' + brkts(num) + ')?'),
    // Decode base64
    re('atob' + parens(quote(base64))),
    // Special case of escape("".italics())[2] which equals "C"
    /(?:Function\("return escape"\)\(\))?\(\(""\)\["italics"\]\(\)\)\[\d+\]/g,
    // Concatenate strings
    /(?<=[(+]\s*)(?:\+*\s*"[^\\"]*?"\s*\+\s*"[^\\"]*?")+/g,
    // Special case of e.g. 20["to" + String["name"]](21)
    /\d+\s*\["to"\s*\+\s*String\["name"\]\]\(\d+\)(?:\[\d+\])?/g
  ];

  // Special case of i.e. g(123) where g = String.fromCharCode
  match = js.match(/^[; ]*([A-za-z])\s*=\s*String\.fromCharCode/m);
  if (match) {
    letter = match[1];
    global[letter] = String.fromCharCode;

    regex.push(re('(?<=[(' + op + '\\s*)' + letter + parens(num)));
  }

  // Special case of atob polyfill
  match = js.match(/^[; ]*([A-za-z])\s*=\s*atob;/m);
  if (match) {
    letter = match[1];
    global[letter] = atob;

    regex.push(re('(?<=[(' + op + '\\s*)' + letter + parens(quote(base64))));
  }

  return regex;
}

function simplify(content) {
  var js = decode(content);
  // js = atob.toString() + '\n\n' + 'var t = "example-site.dev"\n\n' + js;

  var stopBreaking = 's,t,o,p,b,r,e,a,k,i,n,g'.split(',').join('\\s*,\\s*');
  var re = new RegExp(stopBreaking + '(?:,\\s*[a-z_$][a-z_$0-9]*\\s*)+=\\s*\\{'
    + '[\S\s]*?"([a-z]+)"\\s*:\\s*(\\d[.0-9]*)', 'i');

  js.replace(re, function(str, name, value) {
    var num = eval(value);
    var expr = new RegExp('[a-z_$][a-z_$0-9]*\\.' + name
      + '\\s*([+\\-\\/*]=)\\s*(\\d[.0-9]*)\\s*;', 'gi');

    js = js.replace(expr, diffable(function(assignment, operator, operand) {
      try {
        eval('num' + operator + operand);
      } catch (e) {
        return assignment;
      }
      return '';
    }));

    js = js.replace(str, str.replace(new RegExp(':\\s*' + value), ': ' + num));
  });

  return js;
}

function decode(js) {
  if (-1 !== js.indexOf('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=')) {
    // Replace the atob poly fill
    js = js.replace(/function(?:[^}]+>>\s*16\s*&\s*255){3}[^{]+\}[^{}]+\};?/, 'atob;/* <- polyfill */');
  }

  var result = js;
  var diff;

  var regex = getRegex(js);

  regex.forEach(re => {
    result = result.replace(re, diffable(replacer));
  });

  if (result === js) {
    return result;
  }

  return decode(result);
}

function news() {
  if (notices.length > 0) {
    notices.forEach(function (o) {
      var d = new Date(o.date);
      var dateString = '[ ' + d.toLocaleDateString() + ' ' + d.toLocaleTimeString() + ' ]: ';

      console.error(dateString, o.message);
    });
  }
  else {
    console.error('No recent changes have been reported as of yet, PR\'s welcome!');
  }

  process.exit(0);
}

function diffable(replacer) {
  return function(value) {
    var replacement = replacer.apply(this, arguments);

    if (verbose && value !== replacement) {
      if (replacement.length > 0) {
        console.log('Replacement: ' + chalk.green(replacement) + ' === ' + chalk.red(value));
      }
      else {
        console.log('Simplifying: ' + chalk.yellow(value));
      }
    }

    return replacement;
  };
}
